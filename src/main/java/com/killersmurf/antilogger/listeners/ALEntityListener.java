/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.antilogger.listeners;

import com.killersmurf.antilogger.AntiLogger;
import com.topcat.npclib.entity.HumanNPC;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;


/**
 *
 * @author Dylan
 */
public class ALEntityListener implements Listener {

    private AntiLogger al;

    public ALEntityListener(AntiLogger al) {
        this.al = al;
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event instanceof EntityDamageByEntityEvent)) {
            return;
        }
        //Casts the event into a entity damage by entity event.
        final EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
        if (e.getEntity() instanceof Player) {
            //Checks if it is a NPC
            if (al.nm.isNPC(e.getEntity())) {
                //Does weird stuff in they have it so the NPC attacks back.
                if (al.defendSelf) {
                    final HumanNPC npc = (HumanNPC) al.nm.getNPC(al.nm.getNPCIdFromEntity(e.getEntity()));
                    int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(al, new Runnable() {

                        public void run() {
                            al.attackEntity(npc, (Player) e.getDamager());
                            Bukkit.getScheduler().scheduleSyncDelayedTask(al, new Runnable() {

                                public void run() {
                                    Bukkit.getScheduler().cancelTask(al.tasks.get((Player) e.getDamager()));
                                }
                            }, 50L);
                        }
                    }, 0L, 20L);
                    al.tasks.put((Player) e.getDamager(), task);

                }
                return;
            }
            final Player player = (Player) e.getEntity();
            al.battleStatus.put(player, Boolean.TRUE);
            Integer prevTaskId = al.tasks.get(player);

            if (prevTaskId != null && prevTaskId != -1) {
                Bukkit.getScheduler().cancelTask(prevTaskId);
            }
            //Schedule the battle timer
            int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(al, new Runnable() {

                public void run() {
                    al.battleStatus.put(player, Boolean.FALSE);
                    al.tasks.put(player, -1);
                }
            }, 200L); //Approx 10 seconds
            al.tasks.put(player, taskId);
        }
        if (e.getDamager() instanceof Player) {
            if (al.nm.isNPC(e.getDamager())) {
                return;
            }
            final Player player = (Player) e.getDamager();

            al.battleStatus.put(player, Boolean.TRUE);

            Integer prevTaskId = al.tasks.get(player);
            if (prevTaskId != null && prevTaskId != -1) {
                Bukkit.getScheduler().cancelTask(prevTaskId);
            }
            //Schedule the battle timer
            int taskId = Bukkit.getScheduler().scheduleSyncDelayedTask(al, new Runnable() {

                public void run() {

                    al.battleStatus.put(player, Boolean.FALSE);

                    al.tasks.put(player, -1);
                }
            }, 200L); //Approx 10 seconds
            al.tasks.put(player, taskId);
        }
    }
}
