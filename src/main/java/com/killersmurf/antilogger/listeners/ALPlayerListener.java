
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.killersmurf.antilogger.listeners;

import com.killersmurf.antilogger.AntiLogger;
import com.topcat.npclib.entity.HumanNPC;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author Dylan
 */
public class ALPlayerListener implements Listener {

    private AntiLogger al;

    public ALPlayerListener(AntiLogger al) {
        this.al = al;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        //Returns if they are in game mode.(add permission support here)
        if (player.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (al.deadPlayers.contains(player.getName())) {
            return;
        }
        //Checks if there is a Player nearby.
        if (!al.alwaysSpawn && !player.getNearbyEntities(10, 5, 10).isEmpty()) {
            for (Entity entities : player.getNearbyEntities(10, 5, 10)) {
                if (!(entities instanceof Player)) {
                    continue;
                }
                al.spawnHumanNPC(player);
                al.battleStatus.put(player, Boolean.FALSE);
                break;
            }
            return;
        }
        al.spawnHumanNPC(player);
        al.battleStatus.put(player, Boolean.FALSE);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        String playerName = player.getName();
        Inventory inventory = player.getInventory();
        HumanNPC npc = al.nm.getOneHumanNPCByName(playerName);
        if (npc == null && !al.isDead(playerName)) {
            return;
        }
        if (al.isDead(playerName)) {
            al.nm.despawnHumanByName(playerName);
            inventory.clear();
            player.getInventory().setArmorContents(null);
            player.teleport(player.getWorld().getSpawnLocation());
            player.setHealth(20);
            player.setFoodLevel(20);
            al.removeDead(playerName);
            return;
        }
        //Updates the players inventory (McMMo conflicted with disarm but this fixes it)
        inventory.setContents(npc.getInventory().getContents());
        player.getInventory().setArmorContents(npc.getInventory().getArmorContents());
        al.nm.despawnHumanByName(playerName);
    }

    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        //Removes them from the dead list.
        if (al.isDead(event.getPlayer().getName())) {
            al.removeDead(event.getPlayer().getName());
        }
    }
}