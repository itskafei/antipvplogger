package com.killersmurf.antilogger;

import com.killersmurf.antilogger.listeners.ALEntityListener;
import com.killersmurf.antilogger.listeners.ALPlayerListener;
import com.topcat.npclib.NPCManager;
import com.topcat.npclib.entity.HumanNPC;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * 
 * @author Dylan_Holmes
 */
public class AntiLogger extends JavaPlugin {

    //The battle status Hashmap, assigns a boolean to a Player.
    public Map<Player, Boolean> battleStatus = new HashMap<Player, Boolean>();
    //The task hashmap, assigns a TaskId(Integer) to a player.
    public Map<Player, Integer> tasks = new HashMap<Player, Integer>();
    //Creates a NPCManager pointer.
    public NPCManager nm;
    final int x = 5;
    //Creates a statis AntiLogger pointer.
    //Creates a static final logger.
    static final Logger logger = Logger.getLogger("Minecraft");
    //Creates a list of strings, called dead players.
    public List<String> deadPlayers = new ArrayList<String>();
    public boolean alwaysSpawn;
    public boolean defendSelf;
    public long time;

    //This is what happens when the plugin is Disabled.
    @Override
    public void onDisable() {
        //Logger.info("Sends a Message that is with the qoutes");
        logger.info("[AntiLogger] Deleting NPCs...");
        //Despawns all the NPCs.
        nm.despawnAll();
        logger.info("[AntiLogger] NPCs deleted..");
        logger.info("[AntiLogger] Disabled.");
    }

    //This is what happens when the plugin is enabled.
    @Override
    public void onEnable() {
        //Loads the config file, gets the config file contents, and sets the message boolean to what ever the config file says it is.
        load();
        //Sets the playerListener pointer to a new instance of the ALPlayerListener class.I am lazy.
        ALPlayerListener playerL = new ALPlayerListener(this);
        ALEntityListener entityL = new ALEntityListener(this);
        Bukkit.getPluginManager().registerEvents(entityL, this);
        Bukkit.getPluginManager().registerEvents(playerL, this);
        //Sets the nm pointer to a new instance of the NPCManager.
        nm = new NPCManager(this);
        logger.info("[AntiLogger] Enabled.");
    }

    /**
     * Spawns a NPC with the name of the player it is given. 
     * Then copies the inventory contents of the player, and adds it to the NPCs contents. Same for the armour.
     * 
     * @param player 
     */
    public void spawnHumanNPC(final Player player) {
        //Creating Pointers
        String playerName = player.getName();
        ItemStack[] copiedArmour = player.getInventory().getArmorContents();
        ItemStack[] copiedContents = player.getInventory().getContents();

        final HumanNPC playerNPC = (HumanNPC) nm.spawnHumanNPC(playerName, player.getLocation());
        playerNPC.getInventory().setArmorContents(copiedArmour);
        playerNPC.getInventory().setContents(copiedContents);
        playerNPC.updateInventory();


        //Creating a Task...
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this,
                new Runnable() {

                    @Override
                    public void run() {
                        nm.despawnHumanByName(playerNPC.getName());
                    }
                }, (time * 20L));
    }

    /**
     * Checks to see if the List of strings called "deadPlayers", contains a given player name.
     * 
     * @param name
     * @return True, if it does contain, false if it does not.
     */
    public boolean isDead(String name) {
        if (!deadPlayers.contains(name)) {
            return false;
        }
        return true;
    }

    /**
     * Takes in a players name, and adds it to the list of dead players.
     * 
     * @param name 
     */
    public void addDead(String name) {
        if (deadPlayers.contains(name)) {
            return;
        }
        deadPlayers.add(name);
    }

    /**
     * Removes a players name from the "deadPlayers" list.
     * 
     * @param name 
     */
    public void removeDead(String name) {
        deadPlayers.remove(name);
    }

    /**
     * Loads the config variables, and saves the config.
     * 
     */
    public void load() {
        String npcPath = "npc";
        //Defending...
        getConfig().addDefault(npcPath + ".defendSelf", false);
        getConfig().set(npcPath + ".defendSelf", getConfig().getBoolean(npcPath + ".defendSelf"));
        defendSelf = getConfig().getBoolean(npcPath + ".defendSelf");
        //Spawning...
        getConfig().addDefault(npcPath + ".alwaysSpawn", false);
        getConfig().set(npcPath + ".alwaysSpawn", getConfig().getBoolean(npcPath + ".alwaysSpawn"));
        alwaysSpawn = getConfig().getBoolean(npcPath + ".alwaysSpawn");
        //Spawning Time....
        getConfig().addDefault(npcPath + ".time", 15);
        getConfig().set(npcPath + ".time", getConfig().getLong(npcPath + ".time"));
        time = getConfig().getLong(npcPath + ".time");
        //Info alerts...
        getConfig().options().copyDefaults(true);
        this.saveConfig();
    }
    //Makes a NPC attack a player.
    public void attackEntity(final HumanNPC attacker, final Player attacked) {
        final int task = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            public void run() {

                if (attacker.getBukkitEntity().getLocation().distance(attacked.getLocation()) < 6 && nm.getNPCs().contains(attacker)) {
                    attacker.walkTo(attacked.getLocation());
                    attacked.damage(2);
                }
                Bukkit.getScheduler().scheduleSyncDelayedTask(AntiLogger.this, new Runnable() {

                    public void run() {
                        Bukkit.getScheduler().cancelTask(tasks.get(attacked));
                        tasks.remove(attacked);
                    }
                }, 250L);
            }
        }, 5L, 20L);
        tasks.put(attacked, task);
    }
}